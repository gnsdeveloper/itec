<!DOCTYPE html>
<html lang="en">
<head>

	<meta charset="UTF-8">
	<title>ITEC</title>
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link href='http://fonts.googleapis.com/css?family=Open+Sans:400,700,400italic,700italic&subset=cyrillic,cyrillic-ext,latin' rel='stylesheet' type='text/css'>
	<link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">
	<link rel="stylesheet" href="<?php echo esc_url( get_stylesheet_directory_uri() ); ?>/styles/dist/built.min.css">

</head>
<body>
	
	<div class="container" id="main">

		<!-- Header -->

		<div class="header col-md-12 col-sm-12 col-xs-12" id="header-wrapper">

			<div class="header-menu col-md-12 col-sm-12 col-xs-12">
				<ul class="header-menu-container" id="menu">
					<li><a href="#about">Кто мы?</a></li>
					<li><a href="#coop">Кто уже с нами?</a></li>
					<li><a href="#benef">Наши преимущества</a></li>
				</ul>
			</div>

			<div class="header-container">
				<img src="<?php echo esc_url( get_stylesheet_directory_uri() ); ?>/img/logo.png" alt="logo-ITEC" class="logo img-responsive" width="263" height="93">
				<p>Общественная организация «Ассоциация образовательных инициатив» объединила лидеров рынка IT-образования в Украине, их колоссальный опыт, знания и ресурсы ради общей цели – сделать Украину сильной и процветающей IT-страной.</p>
				<a class="btn" href="#memb-ship">Вступить в ассоциацию</a>
				<div class="slide-btn-wrapper">
					<a href="#about" class="slide_btn">
						<img src="<?php echo esc_url( get_stylesheet_directory_uri() ); ?>/img/slide_btn.png" width="29" height="44">
					</a>
				</div>
			</div>

		</div>