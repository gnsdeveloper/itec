<?php get_header(); ?>
		<!-- Top Fixed Menu -->

		<?php include('fixed_menu.php'); ?>	

			<!-- Content -->
		
		<div class="content col-md-12 col-sm-12 col-xs-12">	

			<?php get_sidebar(); ?>			

			<!-- Content Container -->	

			<div class="content-container col-md-9 col-sm-9 col-xs-9">

				<div class="mission col-md-12 col-sm-12 col-xs-12" id="about">

					<div class="mission-item col-md-12 col-sm-12 col-xs-12">
						<div class="col-md-2 col-sm-2 col-xs-2">
							<div class="mission-icon icon-horse"></div>
						</div>
						<div class="description col-md-10 col-sm-10 col-xs-10">
							<h1>Наша миссия</h1>
							<p>Сделать Украину сильной и процветающей IT-страной.</p>
						</div>
					</div>

					<div class="mission-item col-md-12 col-sm-12 col-xs-12">
						<div class="col-md-2 col-sm-2 col-xs-2">
							<div class="mission-icon icon-aim"></div>
						</div>
						<div class="description col-md-10 col-sm-10 col-xs-10">
							<h1>Наши цели </h1>
							<p>Объединить усилия ради успешного и быстрого развития сферы информационных технологий в Украине.</p>
						</div>
					</div>

				</div>

				<div class="obligations col-md-12 col-sm-12 col-xs-12">

					<h1>Участники организации обязуются:</h1>

					<div class="obligations-item col-md-4 col-sm-12 col-xs-12">
						<div class="col-md-12 col-sm-12 col-xs-12">
							<div class="obligations-icon icon-rup"></div>
						</div>
						<h2>Популяризовать</h2>
						<p>IT-образование в Украине, повышать его качество и доступность.</p>
					</div>

					<div class="obligations-item col-md-4 col-sm-12 col-xs-12">
						<div class="col-md-12 col-sm-12 col-xs-12">
							<div class="obligations-icon icon-cv"></div>
						</div>
						<h2>Содействовать</h2>
						<p>трудоустройству специалистов в сфере IT</p>
					</div>

					<div class="obligations-item col-md-4 col-sm-12 col-xs-12">
						<div class="col-md-12 col-sm-12 col-xs-12">
							<div class="obligations-icon icon-active"></div>
						</div>
						<h2>Активно</h2>
						<p>взаимодействовать, обмениваться опытом и информационными материалами с другими образовательными проектами как внутри страны, так и за ее пределами</p>
					</div>

					<div class="obligations-item col-md-4 col-sm-12 col-xs-12">
						<div class="col-md-12 col-sm-12 col-xs-12">
							<div class="obligations-icon icon-coop"></div>
						</div>
						<h2>Сотрудничать</h2>
						<p>в организации и проведении образовательных мероприятий: семинаров, лекций и круглых столов</p>
					</div>

					<div class="obligations-item col-md-4 col-sm-12 col-xs-12">
						<div class="col-md-12 col-sm-12 col-xs-12">
							<div class="obligations-icon icon-promo"></div>
						</div>
						<h2>Способствовать</h2>
						<p>созданию сообщества выпускников и преподавателей для общения, обмена опытом и полезной информацией.</p>
					</div>

					<div class="obligations-item col-md-4 col-sm-12 col-xs-12">
						<div class="col-md-12 col-sm-12 col-xs-12">
							<div class="obligations-icon icon-provide"></div>
						</div>
						<h2>Предоставлять</h2>
						<p>консультационную, информационную, юридическую и экспертную помощь членам ассоциации.</p>
					</div>
					
				</div>

				<div class="cooperation col-md-12 col-sm-12 col-xs-12" id="coop">
					<h1>Уже с нами</h1>
					<p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem. Nulla consequat massa quis enim. Donec pede justo, fringilla vel, aliquet nec, vulputate eget, arcu. In enim justo, rhoncus ut, imperdiet a, venenatis vitae, justo. Nullam dictum felis eu pede mollis pretium. </p>
					<img src="<?php echo esc_url( get_stylesheet_directory_uri() ); ?>/img/partners.png" class="img-responsive">					
				</div>

				<div class="benefits col-md-12 col-sm-12 col-xs-12" id="benef">
					<h1>Преимущества вступления в ITEC</h1>
					<p>Присоединившись к ITEC, ваша компания:</p>

					<div class="benefits-row col-md-12 col-sm-12 col-xs-12">
						<div class="benefits-item col-md-4 col-sm-12 col-xs-12">
							<div class="benefits-icon-container col-md-12 col-sm-12 col-xs-12">
								<div class="benefits-icon icon-1"></div>
							</div>
							<h2>Cтанет</h2>
							<p>полноправным участником команды единомышленников, которые всем сердцем болеют за будущее своей страны</p>
						</div>

						<div class="benefits-item col-md-4 col-sm-12 col-xs-12">
							<div class="benefits-icon-container col-md-12 col-sm-12 col-xs-12">
								<div class="benefits-icon icon-2"></div>
							</div>
							<h2>получит</h2>
							<p>доступ к лучшим отечественным и мировым практикам обучения, информационным материалам, что поможет вывести процесс обучения на качественно новый уровень</p>
						</div>

						<div class="benefits-item col-md-4 col-sm-12 col-xs-12">
							<div class="benefits-icon-container col-md-12 col-sm-12 col-xs-12">
								<div class="benefits-icon icon-3"></div>
							</div>
							<h2>сможет</h2>
							<p>рассчитывать на консультационную, информационную, юридическую и экспертную помощь лидеров рынка IT-образования</p>
						</div>
					</div>

					<div class="benefits-row col-md-12 col-sm-12 col-xs-12">
						<div class="benefits-item col-md-4 col-sm-12 col-xs-12">
							<div class="benefits-icon-container col-md-12 col-sm-12 col-xs-12">
								<div class="benefits-icon icon-4"></div>
							</div>
							<h2>расширит</h2>
							<p>горизонты трудоустройства своих выпускников</p>
						</div>

						<div class="benefits-item col-md-4 col-sm-12 col-xs-12">
							<div class="benefits-icon-container col-md-12 col-sm-12 col-xs-12">
								<div class="benefits-icon icon-5"></div>
							</div>
							<h2>Скоро</h2>
							<p>сможет привлечь дополнительные инвестиции для развития и поддержку со стороны государства</p>
						</div>

						<div class="benefits-item col-md-4 col-sm-12 col-xs-12">
							<div class="benefits-icon-container col-md-12 col-sm-12 col-xs-12">
								<div class="benefits-icon icon-6"></div>
							</div>
							<h2>встанет</h2>
							<p>у истоков создания единого независимого центра тестирования для подтверждения квалификации IT-специалистов</p>
						</div>
					</div>
				</div>

				<div class="membership col-md-12 col-sm-12 col-xs-12" id="memb-ship">

					<div class="membership-container" id="Content">

						<!--Tabs List-->

						<ul class="membership-tabs "id="Tabs">
							<li id="firstTab" class="SelectedTab">
								<a href="#one" onclick="one(); return false;">Стать членом ассоциации</a>
							</li>
							<li id="secondTab" class="Tab">
								<a href="#two" onclick="two(); return false;">Стать партнером</a>
							</li>
						</ul>

						<div class="first-tab" id="one">

							<p>Двери ассоциации открыты для всех образовательных компаний, которые разделяют наши цели и задачи. Заполните анкету на вступление. В ближайшее время мы рассмотрим возможность присоединения к нам в качестве полноправного участника ассоциации и обязательно свяжемся с вами.</p>
							<div class="row col-md-12 col-sm-12 col-xs-12">
								<form action="" class="col-md-8 col-sm-12 col-xs-12">
									<div class="form-part col-md-6 col-sm-12 col-xs-12">
										<input type="text" placeholder="Название компании">
										<input type="url" placeholder="Url компании"> 
										<input type="tel" placeholder="Телефон">
										<input type="email" placeholder="Email">
										<input type="text" placeholder="Контактное лицо">
									</div>
									<div class="form-part col-md-6 col-sm-12 col-xs-12">
										<textarea class="more-info" type="text" placeholder="Сопроводительный текст"></textarea>
										<input class="chose-btn" type="file" value="Обзор...">
										<a class="btn" href="#">Отправить заявку</a>
									</div>
								</form>
								<div class="callback col-md-4 col-sm-12 col-xs-12">
									<h1>Или свяжитесь с нами:</h1>
									<p>тел.:<span>(095) 111-22-33</span></p>
									<p>e-mail: <a href="">itec@community.com</a></p>
								</div>
							</div>	

						</div>

						<div class="second-tab" id="two">

							<p>Двери ассоциации открыты для всех образовательных компаний, которые разделяют наши цели и задачи. Заполните анкету на вступление. <!-- В ближайшее время мы рассмотрим возможность присоединения к нам в качестве полноправного участника ассоциации и обязательно свяжемся с вами. --></p>
							<div class="row col-md-12 col-sm-12 col-xs-12">
								<form action="" class="col-md-8 col-sm-6 col-xs-12">
									<div class="form-part col-md-6 col-sm-6 col-xs-12">
										<input type="text" placeholder="Название компании">
										<input type="url" placeholder="Url компании"> 
										<input type="tel" placeholder="Телефон">
										<input type="email" placeholder="Email">
										<input type="text" placeholder="Контактное лицо">
									</div>
									<div class="form-part col-md-6 col-sm-6 col-xs-12">
										<textarea class="more-info" type="text" placeholder="Сопроводительный текст"></textarea>
										<input class="chose-btn" type="file" value="Обзор...">
										<a class="btn" href="#">Отправить заявку</a>
									</div>
								</form>
								<div class="callback col-md-4 col-sm-6 col-xs-12">
									<h1>Или свяжитесь с нами:</h1>
									<p>тел.:<span>(095) 111-22-33</span></p>
									<p>e-mail: <a href="">itec@community.com</a></p>
								</div>
							</div>			
							
						</div>

					</div>

				</div>

				<div class="partners col-md-12 col-sm-12 col-xs-12">
					<h1>Наши партнеры</h1>
					<img src="<?php echo esc_url( get_stylesheet_directory_uri() ); ?>/img/footer_partners.png" class="img-responsive" width="588" height="79">
				</div>

<?php get_footer(); ?>

				