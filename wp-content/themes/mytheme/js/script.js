var headerH = document.documentElement.clientHeight;
var headerWrapper = document.getElementById('header-wrapper');
document.getElementById('side-menu').style.height = headerH + 'px';
headerWrapper.style.height = headerH + 'px';

$(window).resize(function() {
	var headerH = document.documentElement.clientHeight;
	var headerWrapper = $('#header-wrapper');
  $('#side-menu').height(headerH);

	$(headerWrapper).height(headerH);

});

//------------ScrollTo-----------//

$(document).ready(function(){
	$("#header-wrapper, #menu, #side-menu, #fixed-menu, #sidebMenu").on("click","a", function (event) {		
		event.preventDefault();
		var id  = $(this).attr('href')
		var	top = $(id).offset().top;
		$('body,html').animate({scrollTop: top - 40 +'px'}, 1000);
	});
  var headerH = document.documentElement.clientHeight;
  var menu = $('#side-menu');  
  $(window).scroll(function(event) {
    /* Act on the event */
    if (menu.hasClass('hidden') && window.pageYOffset < headerH){

    }
  });
});


//--------------Tabs---------------//
// 1
function one()
{
  // Табы
  document.getElementById('firstTab').className = 'SelectedTab';
  document.getElementById('secondTab').className = 'Tab';  
 
  // Страницы
  document.getElementById('one').style.display = 'block';
  document.getElementById('two').style.display = 'none';
 
}
// 2
function two()
{
  // Табы
  document.getElementById('firstTab').className = 'Tab';
  document.getElementById('secondTab').className = 'SelectedTab';
 
  // Страницы
  document.getElementById('one').style.display = 'none';
  document.getElementById('two').style.display = 'block';
 
}