module.exports = function(grunt) {

  grunt.initConfig({

    // Remove built directory
    clean: {
      build: ['layout/']
    },
    
    sprite:{
      all: {
        src: 'images/src/*.png',
        dest: 'images/sprite/spritesheet.png',
        destCss: 'styles/dist/sprites.css',
        cssVarMap: function (sprite) {
          var regexp = /-hover/gi;
          var regexp2 = /:hover/gi;
          if (sprite.name.match(regexp)){
            var matches = sprite.name.replace(regexp, ':hover');
            //sprite.name.replace('-hover',':hover');
            sprite.name = matches + ', li:hover .icon-' + matches.replace(regexp2, '') + ', .active .icon-' + matches.replace(regexp2, '');
            console.log(matches);
          } else {
            sprite.name = sprite.name;
          }

        }
      }
    },

    concat: {
        basic: {
            src: ['js/src/*/*.js', 'js/src/*.js'],
            dest: 'js/dist/main.min.js'
        },
        extras: {
            src: ['styles/src/*/*.less'],
            dest: 'styles/dist/built.less'
        }
    },
    less: {
      development: {
        files: {
          'styles/dist/result.css' : ['styles/dist/built.less', 'styles/dist/sprites.less']
        }
      }
    },
    // Build the site using grunt-includes
    includes: {
      build: {
        //cwd: '/',
        src: [ '*.html' ],
        dest: 'layout/',
        options: {
          flatten: false,
          includePath: 'partials'
          //banner: '<!-- <% includes.files.dest %> -->\n'
        }
      }
    },
    watch: {
      files: ['*.html','partials/*.html','js/src/*.js', 'styles/src/*/*.less', 'images/src/*/*.*'],
      tasks: ['clean','includes','sprite','concat','less']
    }
  });

  grunt.loadNpmTasks('grunt-spritesmith');
  grunt.loadNpmTasks('grunt-contrib-concat');
  //grunt.loadNpmTasks('grunt-contrib-cssmin');

  grunt.loadNpmTasks('grunt-contrib-less');
   //grunt.loadNpmTasks('grunt-combine-media-queries');
  grunt.loadNpmTasks('grunt-includes');
  grunt.loadNpmTasks('grunt-contrib-watch');
  grunt.loadNpmTasks('grunt-contrib-clean');


  grunt.registerTask('default', ['clean','includes','concat','less','sprite', 'watch']);

};