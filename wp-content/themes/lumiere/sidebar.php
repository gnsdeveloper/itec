<div class="col-md-3 sidebar right-sidebar">		
			<section class="onlineService">
				<!-- <div class="container"> -->
					<h2>Онлайн сервисы</h2>
					<ul class="container">
						<li class="col-sm-12 col-xs-12 col-md-1">
							<a href="#">
								<div class="icon icon-o1"></div>
								<span>Авиабилеты</span>
							</a>
						</li>
						<li class="col-sm-12 col-xs-12 col-md-1">
							<a href="#">
								<div class="icon icon-o2"></div>
								<span>Ж/Д билеты</span>
							</a>
						</li>
						<li class="col-sm-12 col-xs-12 col-md-1">
							<a href="#">
								<div class="icon icon-o3"></div>
								<span>Автобусные билеты</span>
							</a>
						</li>
						<li class="col-sm-12 col-xs-12 col-md-1">
							<a href="#">
								<div class="icon icon-o4"></div>
								<span>Бронирование отеля</span>
							</a>
						</li>
						<li class="col-sm-12 col-xs-12 col-md-1">
							<a href="#">
								<div class="icon icon-o5"></div>
								<span>Поиск туров</span>
							</a>
						</li>
						<li class="col-sm-12 col-xs-12 col-md-1">
							<a href="#">
								<div class="icon icon-o6"></div>
								<span>Прокат авто</span>
							</a>
						</li>
						<li class="col-sm-12 col-xs-12 col-md-1">
							<a href="#">
								<div class="icon icon-o7"></div>
								<span>Страхование</span>
							</a>
						</li>
					</ul>
				<!-- </div> -->
			</section>
			<a href="#"><img src="<?php echo esc_url( get_stylesheet_directory_uri() ); ?>/images/content/banner.png"></a>
</div>