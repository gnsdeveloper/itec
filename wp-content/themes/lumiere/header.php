<!DOCTYPE html>
<html>
<head>
	<title>test</title>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="stylesheet" type="text/css" href="<?php echo esc_url( get_stylesheet_directory_uri() ); ?>/styles/dist/bootstrap.min.css">	
	<!-- <link rel="stylesheet" type="text/css" href="../node_modules/bootstrap/dist/js/bootstrap.min.js"> -->
	<link rel="stylesheet" type="text/css" href="<?php echo esc_url( get_stylesheet_directory_uri() ); ?>/styles/dist/sprites.css">
	<link rel="stylesheet" type="text/css" href="<?php echo esc_url( get_stylesheet_directory_uri() ); ?>/styles/dist/lightbox.css">
	<link rel="stylesheet" type="text/css" href="<?php echo esc_url( get_stylesheet_directory_uri() ); ?>/styles/dist/result.css">
	<script type="text/javascript" src="<?php echo esc_url( get_stylesheet_directory_uri() ); ?>/js/dist/jquery.min.js"></script>
	<script type="text/javascript" src="<?php echo esc_url( get_stylesheet_directory_uri() ); ?>/js/dist/bootstrap.min.js"></script>
	
</head>
<body>

<header>

<nav class="navbar">
	<div class="container">
		<!-- Brand and toggle get grouped for better mobile display -->
		<div class="navbar-header">
			<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
				<span class="sr-only">Toggle navigation</span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
			</button>
			<a href="#" id="logo" class="icon-logo"></a>
		</div>
		<!-- Collect the nav links, forms, and other content for toggling -->
		<div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
			<?php
			    if ( function_exists( 'wp_nav_menu' ) )
			        wp_nav_menu( 
				        array( 
				        'theme_location' => 'custom-menu',
				        'fallback_cb'=> 'custom_menu',
				        'container' => 'ul',
				        'menu_id' => 'nav',
				        'menu_class' => 'nav navbar-nav') 
					);
			    else custom_menu();
				?>
			<p class="navbar-text navbar-right">+380 94 251 88 88</p>
		</div><!-- /.navbar-collapse -->
	</div><!-- /.container-fluid -->
</nav>
</header>
<div class="breadcrumbs">
	<?php the_breadcrumb(); ?>
</div>
