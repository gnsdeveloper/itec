<?php get_header(); ?>

<div class="wrapper redTour product">
<div class="breadcrumbs">
	<?php the_breadcrumb(); ?>
</div>
</div>
	<div class="container">
		<div class="col-md-9">
			<div class="newsPage">
			<?php if (have_posts()) : ?>
  				<?php while (have_posts()) : the_post(); ?>
					<div class="col-md-12">
						
							<img  src="<?php echo esc_url( get_stylesheet_directory_uri() ); ?>/images/content/news1.png">					
						
							<h4><?php the_title(); ?></h4>
							<p class="light"><?php the_date(); ?></p>
							<p><?php the_excerpt(); ?></p>						

							<a href="<?php the_permalink(); ?>" class="rarr">Показать полностью &rarr;</a>
							
						
					</div>
				<?php endwhile; ?>
			<?php else : ?>
			<?php endif; ?>
				
			</div>

			<div class="container">
				<div class="col-md-12 text-center">
					<?php if(function_exists('wp_pagenavi')) { wp_pagenavi(); } ?>
				</div>
			</div>

		</div>
		
<?php get_sidebar(); ?>

	</div>
</div>

<?php get_footer(); ?>