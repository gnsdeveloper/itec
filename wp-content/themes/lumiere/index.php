<?php get_header(); ?>

<div class="wrapper">
	<section class="topBannerMain">
		<div class="container">
		<div class="icon-womanTopBanner"></div>

		<div class="headlinesMain">
			<h2>Организация</h2> 
			<h1>корпоративных мероприятий</h1>
	 

			<h4>по Украине, СНГ и за рубежом</h4> 

			<a href="#" class="borded">Подробнее</a>

		</div>
			<div class="cont">
			<div class="col-md-4">
				<div class="icon-topBannerImg1">
					<h3>M.I.C.E.</h3>
					<p>Организация бизнес инвент событий</p>
					<a href="#" class="borded">Подробнее</a>
				</div>
			</div>
				
			<div class="col-md-4">
				<div class="icon-topBannerImg3">
					<h3>M.I.C.E.</h3>
					<p>Организация бизнес инвент событий</p>
					<a href="#" class="borded">Подробнее</a>
				</div>
			</div>	
			<div class="col-md-4">
				<div class="icon-topBannerImg2">
					<h3>M.I.C.E.</h3>
					<p>Организация бизнес инвент событий</p>
					<a href="#" class="borded">Подробнее</a>
				</div>
			</div>	

				
			</div>






			
		</div>
	<div class="topScale">
		<div>
			
		</div>
	</div>

	</section>





		<div class="container benefits">
				<div class="col-md-3">
					<div class="icon-benefit3 grayscale"></div>
					<p>15 лет успешной работы</p>
				</div>
				<div class="col-md-3">
					<div class="icon-benefit2 grayscale"></div>
					<p>Аккредитация IATA</p>
				</div >
				<div class="col-md-3">
					<div class="icon-benefit1 grayscale"></div>
					<p>Диплом Амадеус</p>
				</div>
				<div class="col-md-3">
					<div class="icon-benefit4 grayscale"></div>
					<p>15 лет успешной работы</p>
				</div>
		</div>
	<section class="presentation">
		<div class="container">
			<div class="col-md-3">
				<img class="img-responsive img-circle"src="<?php echo esc_url( get_stylesheet_directory_uri() ); ?>/images/content/der.png">
				<p class="text-center">Видео обращение</p>
			</div>
			<div class="col-md-9">
			<div class="icon-topSign"></div>
				<p class="margin-text">
				Добро пожаловать, уважаемые клиенты и гости сайта, в компанию «Lumiere»! Мы с большим удовольствием поможем Вам организовать любое корпоративное мероприятие на территории Украины и за рубежом. Наш многолетний опыт и прифессионализм не оставят Вас без желаемого результата.

				</p>
				<div class="col-md-4">
					<span>С уважением,</span>
					<span class="bold">Лариса Александровна</span>
					<span class="light">Директор компании «Lumiere»</span>
				</div>

				<div class=" col-md-4 ">
				<div class="icon-sign"></div>
					
				</div>
				<div class=" col-md-4 ">
					<a href="#" download class="borded">Скачать нашу призентацию</a>
				</div>
			</div>
		</div>
	</section>
	<section class="onlineService">
		<!-- <div class="container"> -->
			<h2>Онлайн сервисы</h2>
			<ul class="container">
				<li class="col-sm-12 col-xs-12 col-md-1">
					<a href="#">
						<div class="icon icon-o1"></div>
						<span>Авиабилеты</span>
					</a>
				</li>
				<li class="col-sm-12 col-xs-12 col-md-1">
					<a href="#">
						<div class="icon icon-o2"></div>
						<span>Ж/Д билеты</span>
					</a>
				</li>
				<li class="col-sm-12 col-xs-12 col-md-1">
					<a href="#">
						<div class="icon icon-o3"></div>
						<span>Автобусные билеты</span>
					</a>
				</li>
				<li class="col-sm-12 col-xs-12 col-md-1">
					<a href="#">
						<div class="icon icon-o4"></div>
						<span>Бронирование отеля</span>
					</a>
				</li>
				<li class="col-sm-12 col-xs-12 col-md-1">
					<a href="#">
						<div class="icon icon-o5"></div>
						<span>Поиск туров</span>
					</a>
				</li>
				<li class="col-sm-12 col-xs-12 col-md-1">
					<a href="#">
						<div class="icon icon-o6"></div>
						<span>Прокат авто</span>
					</a>
				</li>
				<li class="col-sm-12 col-xs-12 col-md-1">
					<a href="#">
						<div class="icon icon-o7"></div>
						<span>Страхование</span>
					</a>
				</li>
			</ul>
		<!-- </div> -->
	</section>
	<div class="topTourse container">
		<h2>Лучшие туры</h2>
		<div class="col-md-6">
			<div class="tour">		
				<img src="<?php echo esc_url( get_stylesheet_directory_uri() ); ?>/images/content/tour1.png">
				<div class="textZone">
					<a href="#"><h4>Свой остров на Мальдивах</h4></a>
					<div class="col-md-4">
						<p>4 дня/3 ночи</p>
						<p>Нью Йорк-Вашингтон</p>
					</div>
					<div class="col-md-4">
						<ul>
							<span>Тип поездки:</span>
							<li><div class="icon-typeT1 grayscale noColor"></div></li>
							<li><div class="icon-typeT1 grayscale noColor"></div></li>
							<li><div class="icon-typeT1 grayscale noColor"></div></li>
						</ul>
					</div>
					<div class="col-md-4">
						<a href="#" class="borded revers col-md-4">Подробнее</a>
					</div>
				</div>
				<div class="labelT">
					<span>TOP</span> 2015
				</div>
			</div>
		</div>

		<div class="col-md-6">
			<div class="tour">		
			
			<img src="<?php echo esc_url( get_stylesheet_directory_uri() ); ?>/images/content/tour11.png">
			
				<div class="textZone">

					<a href="#"><h4>Свой остров на Мальдивах</h4></a>
					<div>
						<p>4 дня/3 ночи</p>
						<p>Нью Йорк-Вашингтон</p>
					</div>
					<ul>
					<span>Тип поездки:</span>
						<li><div class="icon-typeT1 grayscale noColor"></div></li>
						<li><div class="icon-typeT1 grayscale noColor"></div></li>
						<li><div class="icon-typeT1 grayscale noColor"></div></li>
					</ul>

					<a href="#" class="borded revers">Подробнее</a>
				</div>

				<div class="labelT red">
					<span>BEST</span> SALE
				</div>
			</div>
		</div>
	</div>
	<div class="tourList container">
		<div class="col-md-3 col-sm-6">
			<div class="tour">
				<a class="img-link" href="#"><img src="<?php echo esc_url( get_stylesheet_directory_uri() ); ?>/images/content/tour2.png"></a>
				<div>
					<a href="#">Свой остров на Мальдивах</a>
					<p>4 дня/3 ночи</p>
					<p>Нью Йорк-Вашингтон</p>
				</div>

				<div>
					<ul>
					<span> Тип поездки: </span>
						<li><div class="icon-typeT1 grayscale noColor"></div></li>
						<li><div class="icon-typeT1 grayscale noColor"></div></li>
						<li><div class="icon-typeT1 grayscale noColor"></div></li>
					</ul>
				</div>
			</div>
		</div>
		
	<!-- 	<div class="col-md-3 col-sm-6">
			<div class="tour">
				<a class="img-link" href="#"><img src="<?php echo esc_url( get_stylesheet_directory_uri() ); ?>/images/content/tour21.png"></a>
				<div>
					<a href="#">Свой остров на Мальдивах</a>
					<p>4 дня/3 ночи</p>
					<p>Нью Йорк-Вашингтон</p>
				</div>

				<div>
					<ul>
					<span> Тип поездки: </span>
						<li><div class="icon-typeT1 grayscale noColor"></div></li>
						<li><div class="icon-typeT1 grayscale noColor"></div></li>
						<li><div class="icon-typeT1 grayscale noColor"></div></li>

					</ul>
				</div>
			</div>	
		</div>

		<div class="col-md-3 col-sm-6">
			<div class="tour">
				<a class="img-link" href="#"><img src="<?php echo esc_url( get_stylesheet_directory_uri() ); ?>/images/content/tour22.png"></a>
				<div>
					<a href="#">Свой остров на Мальдивах</a>
					<p>4 дня/3 ночи</p>
					<p>Нью Йорк-Вашингтон</p>
				</div>

				<div>
					<ul>
					<span> Тип поездки: </span>
						<li><div class="icon-typeT1 grayscale noColor"></div></li>
						<li><div class="icon-typeT1 grayscale noColor"></div></li>
						<li><div class="icon-typeT1 grayscale noColor"></div></li>

					</ul>
				</div>
			</div>	
			
		</div>

		<div class="col-md-3 col-sm-6">
			<div class="tour">
				<a class="img-link" href="#"><img src="<?php echo esc_url( get_stylesheet_directory_uri() ); ?>/images/content/tour23.png"></a
				<div>
					<a href="#">Свой остров на Мальдивах</a>
					<p>4 дня/3 ночи</p>
					<p>Нью Йорк-Вашингтон</p>
				</div>

				<div>
					<ul>
					<span> Тип поездки: </span>
						<li><div class="icon-typeT1 grayscale noColor"></div></li>
						<li><div class="icon-typeT1 grayscale noColor"></div></li>
						<li><div class="icon-typeT1 grayscale noColor"></div></li>
					</ul>
				</div>
			</div>	
			
		</div>

		<div class="col-md-3 col-sm-6">
			<div class="tour">
				<a class="img-link" href="#"><img src="<?php echo esc_url( get_stylesheet_directory_uri() ); ?>/images/content/tour24.png"></a>
				<div>
					<a href="#">Свой остров на Мальдивах</a>
					<p>4 дня/3 ночи</p>
					<p>Нью Йорк-Вашингтон</p>
				</div>

				<div>
					<ul>
					<span> Тип поездки: </span>
						<li><div class="icon-typeT1 grayscale noColor"></div></li>
						<li><div class="icon-typeT1 grayscale noColor"></div></li>
						<li><div class="icon-typeT1 grayscale noColor"></div></li>
					</ul>
				</div>
			</div>	
		</div>

		<div class="col-md-3 col-sm-6">
			<div class="tour">
				<a class="img-link" href="#"><img src="<?php echo esc_url( get_stylesheet_directory_uri() ); ?>/images/content/tour25.png"></a
				<div>
					<a href="#">Свой остров на Мальдивах</a>
					<p>4 дня/3 ночи</p>
					<p>Нью Йорк-Вашингтон</p>
				</div>
				<div>
					<ul>
					<span> Тип поездки: </span>
						<li><div class="icon-typeT1 grayscale noColor"></div></li>
						<li><div class="icon-typeT1 grayscale noColor"></div></li>
						<li><div class="icon-typeT1 grayscale noColor"></div></li>
					</ul>
				</div>
			</div>	
			
		</div>

		<div class="col-md-3 col-sm-6">
			<div class="tour">
				<a class="img-link" href="#"><img src="<?php echo esc_url( get_stylesheet_directory_uri() ); ?>/images/content/tour26.png"></a
				<div>
					<a href="#">Свой остров на Мальдивах</a>
					<p>4 дня/3 ночи</p>
					<p>Нью Йорк-Вашингтон</p>
				</div>
				<div>
					<ul>
					<span> Тип поездки: </span>
						<li><div class="icon-typeT1 grayscale noColor"></div></li>
						<li><div class="icon-typeT1 grayscale noColor"></div></li>
						<li><div class="icon-typeT1 grayscale noColor"></div></li>
					</ul>
				</div>
			</div>	
		</div>

		<div class="col-md-3 col-sm-6">
			<div class="tour">
				<a class="img-link" href="#"><img src="<?php echo esc_url( get_stylesheet_directory_uri() ); ?>/images/content/tour27.png"></a
				<div>
					<a href="#">Свой остров на Мальдивах</a>
					<p>4 дня/3 ночи</p>
					<p>Нью Йорк-Вашингтон</p>
				</div>
				<div>
					<ul>
					<span> Тип поездки: </span>
						<li><div class="icon-typeT1 grayscale noColor"></div></li>
						<li><div class="icon-typeT1 grayscale noColor"></div></li>
						<li><div class="icon-typeT1 grayscale noColor"></div></li>
					</ul>
				</div>
			</div>	 -->
			
		</div>
		<div class="col-md-12 col-sm-12 col-xs-12 text-center">
			<a href="#" class="borded revers">Посмотреть все туры</a>
		</div>
	</div>

	<section class="recomendation">
		<div class="container">	
			<h3>Нас рекомендуют</h3>

			<div class="col-md-2 col-sm-3 col-xs-4">
				<img class="img-responsive" src="<?php echo esc_url( get_stylesheet_directory_uri() ); ?>/images/content/rec1.png">
			</div>

			<div class="col-md-2 col-sm-3 col-xs-4">
				<img class="img-responsive" src="<?php echo esc_url( get_stylesheet_directory_uri() ); ?>/images/content/rec2.png">
			</div>

			<div class="col-md-2 col-sm-3 col-xs-4">
				<img class="img-responsive" src="<?php echo esc_url( get_stylesheet_directory_uri() ); ?>/images/content/rec3.png">
			</div>

			<div class="col-md-2 col-sm-3 col-xs-4">
				<img class="img-responsive" src="<?php echo esc_url( get_stylesheet_directory_uri() ); ?>/images/content/rec4.png">
			</div>

			<div class="col-md-2 col-sm-3 col-xs-4">
				<img class="img-responsive" src="<?php echo esc_url( get_stylesheet_directory_uri() ); ?>/images/content/rec5.png">
			</div>

			<div class="col-md-2 col-sm-3 col-xs-4">
				<img class="img-responsive" src="<?php echo esc_url( get_stylesheet_directory_uri() ); ?>/images/content/rec6.png">
			</div>

			<div class="col-md-2 col-sm-3 col-xs-4">
				<img class="img-responsive" src="<?php echo esc_url( get_stylesheet_directory_uri() ); ?>/images/content/rec7.png">
			</div>

			<div class="col-md-2 col-sm-3 col-xs-4">
				<img class="img-responsive" src="<?php echo esc_url( get_stylesheet_directory_uri() ); ?>/images/content/rec8.png">
			</div>

			<div class="col-md-2 col-sm-3 col-xs-4">
				<img class="img-responsive" src="<?php echo esc_url( get_stylesheet_directory_uri() ); ?>/images/content/rec9.png">
			</div>

			<div class="col-md-2 col-sm-3 col-xs-4">
				<img class="img-responsive" src="<?php echo esc_url( get_stylesheet_directory_uri() ); ?>/images/content/rec10.png">
			</div>

			<div class="col-md-2 col-sm-3 col-xs-4">
				<img class="img-responsive" src="<?php echo esc_url( get_stylesheet_directory_uri() ); ?>/images/content/rec11.png">
			</div>

			<div class="col-md-2 col-sm-3 col-xs-4">
				<a href="#" class="borded revers grayscale">Показать еще  &rarr;</a>
			</div>
		</div>
	</section>



		<section class="newsList">

			<div class="container">		
			<h3>Лента новостей</h3>
			<?php if (have_posts()) : ?>
				<?php while (have_posts()) : the_post(); ?>
				<div>
				
						<div class="post col-md-2 col-sm-12">
							<p><?php  the_date(); ?></p>
							<a href="<?php the_permalink(); ?>"><?php the_title(); ?></a>
						</div>						
					
				</div>
				<?php endwhile; ?>
			<?php else : ?>
			<?php endif; ?>	
				<a class="col-md-2 col-sm-12 col-md-offset-10 text-center" href="#">Читать все новости</a>
			</div>
		</section>
	

 </div>

<?php get_footer(); ?>

