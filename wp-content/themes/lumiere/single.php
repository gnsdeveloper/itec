<?php get_header(); ?>

<div class="wrapper redTour product">
<div class="breadcrumbs">
	<?php the_breadcrumb(); ?>
</div>
	<div class="container">
		<div class="col-md-9">
		<?php if (have_posts()) : ?>
  			<?php while (have_posts()) : the_post(); ?>
			<div class="col-md-12 oneNews">
				<h1><?php the_title(); ?></h1>
				<p class="light"><?php the_date(); ?></p>

				<img src="<?php echo esc_url( get_stylesheet_directory_uri() ); ?>/images/content/news11.png">

				<?php the_content(); ?>

				<a href="#">&larr; Назад</a>

			</div>
			<?php endwhile; ?>
		<?php else : ?>
		<?php endif; ?>
			
		</div>
		<?php get_sidebar(); ?>
	</div>
</div>
<?php get_footer(); ?>