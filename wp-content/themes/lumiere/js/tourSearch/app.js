var app = angular.module('searchTour', [
	'ngAnimate',
	'ui.router'
	])
.controller('tourController', function ($scope, $http, $location){
	$scope.days = null;
	$scope.tourType = [];
	$scope.countries = [];
	$scope.disabled = false;
	$scope.searchObject = $location.search();
	$scope.data = [];

	$scope.pageCounter = 0;

	$scope.parseHash = function (){
		$scope.searchObject = $location.search();
		if (!$scope.searchObject.isEmptyObject){
			if ('type' in $scope.searchObject && 'country' in $scope.searchObject && 'days' in $scope.searchObject){
				$scope.tourType = $scope.searchObject.type.split(',');
				$scope.countries = $scope.searchObject.country.split(',');
				$scope.days = parseInt($scope.searchObject.days[0], 10);
			} else if ('type' in $scope.searchObject){
				$scope.tourType = $scope.searchObject.type.split(',');
				$scope.countries = [];
			} else if('country' in $scope.searchObject){
				$scope.countries = $scope.searchObject.country.split(',');
				$scope.tourType = [];
			} else if('days' in $scope.searchObject){
				$scope.days = parseInt($scope.searchObject.days[0], 10);
				$scope.tourType = [];
			} else{
				$scope.tourType = [];
				$scope.countries = [];
			}
			if ( typeof $scope.searchObject.items !== "undefined"){
				$scope.pageSize = parseInt($scope.searchObject.items);
			} else {
				$scope.pageSize = 6;
			}
			// if ($scope.pageSize != 6){
			// 	$scope.pageCounter = $scope.pageSize / 6;
			// } else {
			// 	$scope.pageCounter = 0;
			// }
		}
		$scope.refreshLocation();	
	}

	$scope.updateInterface = function () {
		$http.get('../js/tourSearch/filterData.json').
		success(function(data, status, headers, config) {
			$scope.filterData = data;
			$scope.pageSize = $scope.filterData.perPage;
			for (var property in $scope.filterData.type){
				var elNumber = $.inArray($scope.filterData.type[property].model, $scope.tourType);
				if (elNumber >= 0){
					$scope.filterData.type[property].model = 'active';
				} else {
					$scope.filterData.type[property].model = 'no-active';
				}
			}
			for (var property in $scope.filterData.country){
				var elNumber1 = $.inArray($scope.filterData.country[property].model, $scope.countries);
				if (elNumber1 >= 0){
					var element = $scope.countries[elNumber1];
					$scope.filterData.country[property].model = true;
				} else {
					$scope.filterData.country[property].model = 'noactive';
				}
			}
			$scope.days = $scope.filterData.days;
		}).
		error(function(data, status, headers, config) {
			console.log('error' + data);
		});	
	}

	$scope.clearForm = function (){
		$scope.tourType = [];
		$scope.countries = [];
		$location.url('/');
		$scope.parseHash();
	}

	$scope.getData = function (dataObj){
		console.log(dataObj);
		$http.post('../js/tourSearch/getdata.json', dataObj).
		success(function(data, status, headers, config) {
			angular.forEach(data.tours, function(value, key) {
				this.push(data.tours[key]);
			}, $scope.data);
			$scope.state = data.state;
			$scope.init();
		}).
		error(function(data, status, headers, config) {
			console.log('error' + data);
		});	
	}

	$scope.refreshLocation = function (){
		if ($scope.tourType.length == 0 && $scope.countries.length == 0 && $scope.days <= 0){
			$location.url('?items=' + $scope.pageSize);
			$scope.searchObject = $location.search();
		} else if ($scope.tourType.length > 0 && $scope.countries.length > 0 && $scope.days > 0){
			$location.url('?items=' + $scope.pageSize + '&type=' + $scope.tourType + '&country=' + $scope.countries + '&days=' + $scope.days);
			$scope.searchObject = $location.search();
		} else if ($scope.tourType.length > 0 && $scope.countries.length == 0 && $scope.days <= 0){
			$location.url('?items=' + $scope.pageSize + '&type=' + $scope.tourType);	
			$scope.searchObject = $location.search();
		} else if ($scope.tourType.length == 0 && $scope.countries.length > 0 && $scope.days <= 0){
			$location.url('?items=' + $scope.pageSize + '&country=' + $scope.countries);
			$scope.searchObject = $location.search();
		} else if ($scope.tourType.length == 0 && $scope.countries.length == 0 && $scope.days > 0){
			$location.url('?items=' + $scope.pageSize + '&days=' + $scope.days);
			$scope.searchObject = $location.search();
		} else if ($scope.tourType.length > 0 && $scope.countries.length == 0 && $scope.days > 0){
			$location.url('?items=' + $scope.pageSize + '&type=' + $scope.tourType + '&days=' + $scope.days);	
			$scope.searchObject = $location.search();
		} else if ($scope.tourType.length == 0 && $scope.countries.length > 0 && $scope.days > 0){
			$location.url('?items=' + $scope.pageSize + '&country=' + $scope.countries + '&days=' + $scope.days);
			$scope.searchObject = $location.search();
		}
		console.log(['searchObject', $scope.searchObject])
		$scope.getData($scope.searchObject);
	}

	
	$scope.parseHash();

	$scope.init = function (){
		$scope.includeTour = function(type, country) {
			console.log(type);
			console.log(country);
			if (type){
				var i = $.inArray(type, $scope.tourType);
				if (i > -1) {
					$scope.tourType.splice(i, 1);
				} else {
					$scope.tourType.push(type);
				}
			}
			if (country){
				var a = $.inArray(country, $scope.countries);
				if (a > -1) {
					$scope.countries.splice(a, 1);
				} else {
					$scope.countries.push(country);
				} 
			}
			$scope.refreshLocation();
		}

		$scope.searchEq = function(arr1, arr2) {
			var eqCol = 0;
			for (var i = 0; i <= arr1.length - 1; i++){
				for (var a = 0; a <= arr2.length - 1; a++){
					if (arr1[i] == arr2[a]){
						$scope.counter++;
						eqCol++;
					} 		
				}	
			}
			return eqCol;
		}

		$scope.showMore = function (event){
			if ($scope.pageSize < $scope.filterData.total){
				$scope.pageSize = $scope.pageSize + $scope.filterData.loadItemCol;
			} else {
				$scope.pageSize = $scope.filterData.total;
			}
		}

		$scope.$watch('pageSize', function(newValue, oldValue) {
			if (newValue !== oldValue){
				$scope.refreshLocation();
			}			
		});


	}
});
angular.module('searchTour').filter('pagination', function () {
	return function(input, start)
		{
			start = +start;
			return input.slice(start);
		};
});





